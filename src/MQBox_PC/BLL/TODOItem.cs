﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TODOItem
    {
        public TODOItem(bool isdel, string content)
        {
            IsDel = isdel;
            Content = content;
        }

        public bool IsDel { get; set; }
        public string Content { get; set; }
    }
}
