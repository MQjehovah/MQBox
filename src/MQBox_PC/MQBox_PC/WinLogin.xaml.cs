﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MQBox_PC
{
    /// <summary>
    /// WinLogin.xaml 的交互逻辑
    /// </summary>
    public partial class WinLogin : Window
    {
        private const string IDpre = "账户";
        private const string PSWpre = "密码";

        public WinLogin()
        {
            InitializeComponent();
        }

        #region 无边框窗体拖动
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        #endregion

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string msg = string.Empty;
            if (LoginHandler.TryLogin(txtID.Text, txtPSW.Text, ref msg))
            {
                MainWindow winMain = new MainWindow();
                winMain.Top = this.Top;
                winMain.Left = this.Left;
                winMain.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show(msg);
            }

        }

        private void TextBox_GotFocus_1(object sender, RoutedEventArgs e)
        {
            if (txtID.Text == IDpre)
                txtID.Text = "";
        }

        private void TextBox_GotFocus_2(object sender, RoutedEventArgs e)
        {
            if (txtPSW.Text == PSWpre)
                txtPSW.Text = "";
        }

        private void txtPSW_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
