﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MQBox_PC
{
    /// <summary>
    /// WinDock.xaml 的交互逻辑
    /// </summary>
    public partial class WinDock : Window
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpWindowClass, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);
        const int GWL_HWNDPARENT = -8;
        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        public WinDock()
        {
            InitializeComponent();

        }

        public void SetFocus()
        {
            txtSearch.Focus();
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //IntPtr hWnd = new WindowInteropHelper(this).Handle;
            //IntPtr hWndProgMan = FindWindow("Progman", "Program Manager");
            //SetParent(hWnd, hWndProgMan);
            //List<TODOItem> list = new List<TODOItem>()
            //{
            //   new TODOItem(true,"hello"),
            //   new TODOItem(true,"fuck"),
            //    new TODOItem(false,"nothing")
            //};
            //this.TodoList.ItemsSource = list;

        }

        private void Window_LostFocus(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void txtSearch_LostFocus(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
