﻿using MQBox.Common.KeyBoardHook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MQBox_PC
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        WinDock dockPanel = new WinDock();
        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            Hotkey.Regist(this, HotkeyModifiers.MOD_CONTROL, Key.Space, () =>
            {
                dockPanel.Show();
                dockPanel.SetFocus();
            });
        }

        #region 无边框窗体拖动
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        #endregion

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("text");
            WinDock dock = new WinDock();
            dock.Show();
        }

        private void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

    }
}
